# SQR Lab 9

I will test hackmd.io

## [Forgot password](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)
Here is the test case:
| Test step | Result |
| - | - |
| Visit https://hackmd.io/login | Ok |
| Press on "Forgot password" | Opened https://hackmd.io/settings/forgotPassword |
| Enter email for existing account and click "Reset password" | Ok |
| Check response (using devtools) time and message |  Response time - 387.07 ms, message - "We've sent you a reset password link, please check your email" |
| Now repeat with nonexistent account | |
| Visit https://hackmd.io/login | Ok |
| Press on "Forgot password" | Opened https://hackmd.io/settings/forgotPassword |
| Enter email for nonexistent account (e.g., dskahvgkj@mail.ru) and click "Reset password" | Ok |
| Check response (using devtools) time and message |  Response time - 387.38 ms, message - "We've sent you a reset password link, please check your email" |
| Check that both results are the same | They are indeed the same |

### Summary

Now we can check which OWASP requirements the website follows

| Requirement | Result |
| - | - |
| Return a consistent message for both existent and non-existent accounts | + |
| Ensure that the time taken for the user response message is uniform | + |
| Use a side-channel to communicate the method to reset their password | + (restore link is sent to the email) |

## [Case-insensitive User IDs](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids)

Here is the test case:
| Test step | Result |
| - | - |
| Have an existing account | My login is max.ks04@mail.ru |
| Visit https://hackmd.io/login | Ok |
| Enter "MAX.ks04@mail.ru" as login and a valid password | Ok |
| Chack that the authentication is successful | It was successful |
| Visit https://hackmd.io/join | Ok |
| Enter some username, some password, and email "max.ks04@mail.ru" | Ok |
| Check that the website disallows repeating emails | Get message "This email has been used, please try another one." |

### Summary

Now we can check which OWASP requirements the website follows

| Requirement | Result |
| - | - |
| Usernames/user IDs should be case-insensitive | + |
| Usernames should also be unique | + |

## [HTTP Response Headers](https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html)

Here is the test case:
| Test step | Result |
| - | - |
| Visit https://hackmd.io/login and see response headers | Ok |
| X-Frame-Options header | `SAMEORIGIN` |
| X-XSS-Protection header | `1; mode=block` |
| X-Content-Type-Options header | - |
| Referrer-Policy header | `same-origin` |
| Content-Type header | `text/html; charset=utf-8` |
| Strict-Transport-Security header | `max-age=31536000; includeSubDomains; preload` |
| Expect-CT header | - |
| Content-Security-Policy header | `script-src 'self' vimeo.com https://gist.github.com www.slideshare.net 'unsafe-eval' https://assets.hackmd.io https://www.google.com...` (omitted)
| Access-Control-Allow-Origin header | - |
| Server header | - |
| X-Powered-By header | `Express` |

*other headers are omitted because they are irrelevant to the chosen website

### Summary

Now we can check which OWASP recommendations the website follows

| Recommendation | Result |
| - | - |
| X-Frame-Options: DENY | - |
| Do not set X-XSS-Protection header or explicitly turn it off | - |
| X-Content-Type-Options: nosniff | - |
| Referrer-Policy: strict-origin-when-cross-origin | - |
| Content-Type: text/html; charset=UTF-8 | + |
| Strict-Transport-Security: max-age=63072000; includeSubDomains; preload | + |
| Do not use Expect-CT header | + |
| Configure Content-Security-Policy header | + |
| If you use Access-Control-Allow-Origin header, set specific origins instead of * | + |
| Remove Server header or set non-informative values | + |
| Remove all X-Powered-By headers | - |


